package com.crud.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.crud.demo.entity.students;

@Repository
public interface StudentRepository extends JpaRepository<students, Integer> {

	@Transactional(readOnly = true)
	Optional<students> findByName(String scity);
	
}
