package com.crud.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.crud.demo.dto.studentsDTO;
import com.crud.demo.entity.students;
import com.crud.demo.service.studentService;
import org.apache.log4j.Logger;

@CrossOrigin("*")
@RestController
@RequestMapping("/students")
public class studentController {
	
	private final Logger LOGGER = Logger.getLogger(getClass());
	
	@Autowired
	private studentService studentService;
	
	@GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getStudents(){
		LOGGER.info("Start");
		return ResponseEntity.ok(this.studentService.findAll());
	}
	
	@GetMapping(value = "/find/{description}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> findBySname(@PathVariable("description") String description){
		return ResponseEntity.ok(this.studentService.findByName(description));
	}
	
	@GetMapping(value = "/find/id/{idstudent}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> findById(@PathVariable("idstudent") Integer idstudent){
		return ResponseEntity.ok(this.studentService.findById(idstudent));
	}
	
	@GetMapping(value = "/count")
	public ResponseEntity<Object> count(){
		return ResponseEntity.ok(this.studentService.count());
	}
	
	@GetMapping(value = "/save")
	public ResponseEntity<Object> saveStudent(@RequestBody studentsDTO student){
		this.studentService.save(student);
		return ResponseEntity.ok("ok");
	}
	
	@GetMapping(value = "/paging")
	public Page<students> paging(@RequestParam Integer pageSize, @RequestParam Integer pageNumber){
		return this.studentService.findStudentPage(pageSize,pageNumber);
	}
	

}
