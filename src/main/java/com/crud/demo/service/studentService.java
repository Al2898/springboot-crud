package com.crud.demo.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.crud.demo.dto.studentsDTO;
import com.crud.demo.entity.students;

@Service
public interface studentService {
	
	List<studentsDTO> findAll();
	
	studentsDTO findByName(String scity);
	
	studentsDTO findById(Integer idstudent);
	
	Long count();
	
	void save(studentsDTO student);
	
	Page<students> findStudentPage(Integer pageSize, Integer pageNumber);

}
