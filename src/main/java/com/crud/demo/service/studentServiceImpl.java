package com.crud.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.crud.demo.dto.studentsDTO;
import com.crud.demo.entity.students;
import com.crud.demo.repository.StudentRepository;
import com.crud.demo.utils.MHelper;

@Component
public class studentServiceImpl implements studentService {
	
	private final Logger LOGGER = Logger.getLogger(getClass());

	@Autowired
	private StudentRepository studentRepository;
	
	@Override
	public List<studentsDTO> findAll() {
		
		List<studentsDTO> studentsList = new ArrayList<>();
		LOGGER.info("Creacion de lista");
		Iterable<students> students = this.studentRepository.findAll();
		LOGGER.info("los datos");
		for (students student: students) {
			studentsDTO studentDTO = MHelper.modelMapper().map(student, studentsDTO.class);
			studentsList.add(studentDTO);
		}
		LOGGER.info("fin de los datos");
		return studentsList;
	}

	@Override
	public studentsDTO findByName(String scity) {
		Optional<students> students = this.studentRepository.findByName(scity);
		if(!students.isPresent()) {
			return null;
		}
		
		return MHelper.modelMapper().map(students.get(), studentsDTO.class);
	}


	@Override
	public studentsDTO findById(Integer idstudent) {
		Optional<students> students = this.studentRepository.findById(idstudent);
		if(!students.isPresent()) {
			return null;
		}
		
		return MHelper.modelMapper().map(students.get(), studentsDTO.class);
	}

	@Override
	public Long count() {
		Long cantidad = this.studentRepository.count();
		return cantidad;
	}

	@Override
	public void save(studentsDTO student) {
		students studentSave = MHelper.modelMapper().map(student, students.class);
		this.studentRepository.save(studentSave);
	}

	@Override
	public Page<students> findStudentPage(Integer pageSize, Integer pageNumber) {
		Pageable page = PageRequest.of(pageNumber, pageSize);
		return this.studentRepository.findAll(page);
	}


}
